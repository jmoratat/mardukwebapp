import { Injectable } from '@angular/core';
import Interactions from '@aws-amplify/interactions';

@Injectable()
export class LexService {
  constructor(){}

  async sendMessage(type: string, message: string): Promise<any> {

    //Start Chat Bot Conversation
    let userInput = "Add postit";
    const response1 = await Interactions.send("MardukBot", userInput);
    console.log (response1);
    //Send Type of Postit
    const response2 = await Interactions.send("MardukBot", type);
    console.log (response2);
    //Send the Postit Message
    const response3 = await Interactions.send("MardukBot", message);
    console.log (response3);
    return response3;
    //return this.awsLexProvider.sendMessage('<LEX_BOT_NAME>', message);
  }

} 