import { Component } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import { LexService } from '../services/lex.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  text: string;
  typeoption: string;
  completed = false;
  response: any;

  constructor (private lexService: LexService){}

  async sendPostIt(){
    this.completed = false;
    this.response = await this.lexService.sendMessage(this.typeoption,this.text);
    this.completed = true;
  }
}