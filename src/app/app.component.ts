import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import Amplify, { Auth } from 'aws-amplify';
import aws_exports from '../aws-exports';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {

    Amplify.configure({
      Auth: {
        identityPoolId: 'us-east-1:67815fff-5589-4be1-b778-53bdf0a734ca',
        region: 'us-east-1'
      },
      Interactions: {
        bots: {
          "MardukBot": {
            "name": "MardukBot",
            "alias": "mardukbot",
            "region": "us-east-1",
          },
        }
      }
    });





    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
